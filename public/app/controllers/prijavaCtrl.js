(function () {
    'use strict';

    var prijavaCtrl = function ($firebaseArray, $mdToast, $timeout) {
        var vm = this;

        var ref = firebase.database().ref().child('prijavaiodgovori');
        vm.dataFb = $firebaseArray(ref);

        vm.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAU5i6c8o1Pn7Us5vZ4mSdWjcI0l0D5v2c';

        vm.kategorije = ['Ceste', 'Kanalizacija', 'Rasvjeta i semafori'];
        vm.positions = [];
        vm.showAnimated = false;

        $timeout(function () {
            vm.showAnimated = true;
        }, 800);

        vm.addMarker = function (event) {
            if (vm.positions.length === 0) {
                var ll = event.latLng;
                vm.positions.push({ pos: [ll.lat(), ll.lng()] });
            }
        };
        vm.deleteMarkers = function () {
            vm.positions = [];
        };

        vm.showToast = function (poruka) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(poruka)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        };

        vm.posaljiPrijavu = function () {
            vm.data = {
                pos: vm.positions[0].pos,
                ime: vm.ime,
                prezime: vm.prezime,
                email: vm.email,
                telefon: vm.telefon,
                adresa: vm.adresa,
                kategorija: vm.kategorija,
                opisKvara: vm.opisKvara,
                stanjePrijave: '',
                vrijemePrijave: firebase.database.ServerValue.TIMESTAMP
            };
            vm.dataFb.$add({
                prijava: vm.data
            }).then(function () {
                vm.positions = [];
                vm.ime = '';
                vm.prezime = '';
                vm.email = '';
                vm.telefon = '';
                vm.adresa = '';
                vm.kategorija = '';
                vm.opisKvara = '';

            }).catch(function (error) {
                console.log('Error:', error);
            });
            vm.showToast('Prijava poslana!');
        };

    };

    prijavaCtrl.$inject = ['$firebaseArray', '$mdToast', '$timeout'];

    angular
        .module('prijavaKvara')
        .controller('prijavaCtrl', prijavaCtrl);

} ());