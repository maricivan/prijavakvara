(function () {
    'use strict';

    var pregledDetaljiCtrl = function pregledDetaljiCtrl($scope, $firebaseArray, $stateParams, $state, $mdDialog, $mdMedia, $mdToast, $timeout) {
        var vm = this;

        var ref = firebase.database().ref().child('prijavaiodgovori');
        vm.dataFb = $firebaseArray(ref);

        var list = $firebaseArray(ref);

        vm.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAU5i6c8o1Pn7Us5vZ4mSdWjcI0l0D5v2c';

        vm.id = $stateParams.prijavaId;
        vm.dataMarker = null;
        vm.stanja = ['Zaprimljeno', 'U obradi', 'Otklonjeno'];
        vm.showAnimated = false;
        
        var refPrijava = firebase.database().ref().child('prijavaiodgovori/' + vm.id + '/prijava');
        vm.dataPrijava = $firebaseArray(refPrijava);

        var refOdgovor = firebase.database().ref().child('prijavaiodgovori/' + vm.id + '/odgovori');
        vm.dataFbOdg = $firebaseArray(refOdgovor);

        $timeout(function () {
            vm.showAnimated = true;
        }, 800);

        list.$loaded().then(function () {
            vm.dataFbId = list.$getRecord(vm.id);
        }).catch(function (error) {
            console.log('Error:', error);
        });

        var listOdg = $firebaseArray(refOdgovor);

        listOdg.$loaded().then(function () {
            vm.lastOdg = 0;
            vm.lastOdgovor = {};
            angular.forEach(vm.dataFbOdg, function (value, key) {
                if (value.vrijemeOdgovora > vm.lastOdg) {
                    vm.lastOdg = value.vrijemeOdgovora;
                    vm.lastOdgovor = value;
                }
            });
        }).catch(function (error) {
            console.log('Error:', error);
        });

        list.$loaded().then(function () {
            vm.zaprimljeno = 0;
            vm.uobradi = 0;
            vm.otklonjeno = 0;
            angular.forEach(vm.dataFb, function (value, key) {
                if (value.prijava.stanjePrijave === 'Zaprimljeno') {
                    vm.zaprimljeno += 1;
                } else if (value.prijava.stanjePrijave === 'U obradi') {
                    vm.uobradi += 1;
                } else if (value.prijava.stanjePrijave === 'Otklonjeno') {
                    vm.otklonjeno += 1;
                }
            });
        }).catch(function (error) {
            console.log('Error:', error);
        });

        vm.showDialog = function (ev) {
            vm.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                controller: 'pregledDetaljiCtrl as vm',
                templateUrl: 'dialog1.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen
            })
                .then(function (answer) {
                    vm.status = 'You said the information was "' + answer + '".';
                }, function () {
                    vm.status = 'You cancelled the dialog.';
                }).catch(function (error) {
                    console.log('Error:', error);
                });

            $scope.$watch(function () {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function (wantsFullScreen) {
                vm.customFullscreen = (wantsFullScreen === true);
            });

        };

        vm.zatvori = function () {
            $mdDialog.cancel();
        };

        vm.showToast = function (poruka) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(poruka)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        };

        vm.odgovori = function () {
            vm.data = {
                odgovor: vm.odgovor,
                stanje: vm.stanje,
                vrijemeOdgovora: firebase.database.ServerValue.TIMESTAMP
            };
            vm.dataFbOdg.$add(
                vm.data
            ).catch(function (error) {
                console.log('Error:', error);
            });
            refPrijava.update({
                stanjePrijave: vm.data.stanje
            }).catch(function (error) {
                console.log('Error:', error);
            });
            $mdDialog.cancel();
            vm.showToast('Uspješno ste odgovorili!');
            $state.reload();
        };

    };

    pregledDetaljiCtrl.$inject = ['$scope', '$firebaseArray', '$stateParams', '$state', '$mdDialog', '$mdMedia', '$mdToast', '$timeout'];

    angular
        .module('prijavaKvara')
        .controller('pregledDetaljiCtrl', pregledDetaljiCtrl);

} ());