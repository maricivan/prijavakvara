(function () {
    'use strict';

    var pregledCtrl = function pregledCtrl($firebaseArray, $stateParams, $state, $timeout) {
        var vm = this;

        var ref = firebase.database().ref().child('prijavaiodgovori');
        vm.dataFb = $firebaseArray(ref);

        vm.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAU5i6c8o1Pn7Us5vZ4mSdWjcI0l0D5v2c';

        vm.id = $stateParams.prijavaId;
        vm.dataMarker = null;
        vm.showAnimated = false;

        var list = $firebaseArray(ref);

        $timeout(function () {
            vm.showAnimated = true;
        }, 800);

        vm.changeState = function (a) {
            $state.go('pregledDetail', { 'prijavaId': this.id });
        };

        var num = [];
        list.$loaded().then(function () {
            angular.forEach(vm.dataFb, function (value, key) {
                num.push(value.prijava.vrijemePrijave);
            });
            vm.last = Math.max.apply(null, num);
        }).catch(function (error) {
            console.log('Error:', error);
        });

        list.$loaded().then(function () {
            vm.zaprimljeno = 0;
            vm.uobradi = 0;
            vm.otklonjeno = 0;
            angular.forEach(vm.dataFb, function (value, key) {
                if (value.prijava.stanjePrijave === 'Zaprimljeno') {
                    vm.zaprimljeno += 1;
                } else if (value.prijava.stanjePrijave === 'U obradi') {
                    vm.uobradi += 1;
                } else if (value.prijava.stanjePrijave === 'Otklonjeno') {
                    vm.otklonjeno += 1;
                }
            });
        }).catch(function (error) {
            console.log('Error:', error);
        });

    };

    pregledCtrl.$inject = ['$firebaseArray', '$stateParams', '$state', '$timeout'];

    angular
        .module('prijavaKvara')
        .controller('pregledCtrl', pregledCtrl);

} ());