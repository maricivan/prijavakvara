(function () {
    'use strict';

    var layoutCtrl = function layoutCtrl($mdSidenav) {
        var vm = this;

            vm.toggleSidenav = function (menuId) {
                $mdSidenav(menuId).toggle();
            };

            vm.close = function () {
                $mdSidenav('left').close();
            };

    };

    layoutCtrl.$inject = ['$mdSidenav'];

    angular
        .module('prijavaKvara')
        .controller('layoutCtrl', layoutCtrl);

} ());