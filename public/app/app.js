(function () {
    'use strict';

    angular
        .module('prijavaKvara', [
            'ngMaterial',
            'ngMessages',
            'material.svgAssetsCache',
            'ngAnimate',
            'ngMdIcons',
            'ui.router',
            'ngMap',
            'firebase'
        ])

        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('prijava', {
                    url: '/',
                    templateUrl: 'app/views/prijavaKvara.html',
                    controller: 'prijavaCtrl as vm'
                })
                .state('pregled', {
                    url: '/pregledPrijava',
                    templateUrl: 'app/views/pregledPrijava.html',
                    controller: 'pregledCtrl as vm'
                }).state('pregledDetail', {
                    url: '/pregledPrijava/:prijavaId',
                    templateUrl: 'app/views/pregledPrijavaDetalji.html',
                    controller: 'pregledDetaljiCtrl as vm'
                });

        });
} ());